/*
 * collecty
 * Copyright (C) 2015 IPFire Team (www.ipfire.org)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Python.h>

#include <atasmart.h>
#include <linux/hdreg.h>
#include <mntent.h>
#include <oping.h>
#include <sensors/error.h>
#include <sensors/sensors.h>

#define MODEL_SIZE  40
#define SERIAL_SIZE 20

#define PING_HISTORY_SIZE 1024
#define PING_DEFAULT_COUNT 10
#define PING_DEFAULT_TIMEOUT 8

/* block devices */
typedef struct {
	PyObject_HEAD
	char* path;
	struct hd_driveid identity;
	SkDisk* disk;
} BlockDevice;

extern PyTypeObject BlockDeviceType;

/* ping */
extern PyObject* PyExc_PingError;
extern PyObject* PyExc_PingAddHostError;
extern PyObject* PyExc_PingNoReplyError;

typedef struct {
	PyObject_HEAD
	pingobj_t* ping;
	const char* host;
	struct {
		double history[PING_HISTORY_SIZE];
		size_t history_index;
		size_t history_size;
		size_t packets_sent;
		size_t packets_rcvd;
		double average;
		double stddev;
		double loss;
	} stats;
} PingObject;

extern PyTypeObject PingType;

/* sensors */
typedef struct {
	PyObject_HEAD
	const sensors_chip_name* chip;
	const sensors_feature* feature;
} SensorObject;

extern PyTypeObject SensorType;

PyObject* _collecty_sensors_init();
PyObject* _collecty_sensors_cleanup();
PyObject* _collecty_get_detected_sensors(PyObject* o, PyObject* args);

/* utils */
int _collecty_mountpoint_is_virtual(const struct mntent* mp);
PyObject* _collecty_get_mountpoints();
